import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Time {
	public static void main(String[] args) throws ParseException {
		Scanner sc = new Scanner(System.in);
		String str_date = sc.nextLine();
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		Date date = format.parse(str_date);
		int year = new Integer(new SimpleDateFormat("yyyy").format(date));
		GregorianCalendar gcalendar = new GregorianCalendar();
		 if(gcalendar.isLeapYear(year)) {
	         System.out.println(year + " - високосный год");
	      }else {
	         System.out.println(year + " - не високосный год");
	      }
	}
}
